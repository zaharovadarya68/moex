This application allows you to get information about the activity of users of the MOEX. 
If you want to get information about all firms in a particular period of time, enter the command 'all' in the input file, for example:
command: all
date1:   12.03.2018
date2:   13.04.2018

If you want to get information about users of a specific company, you should enter the command 'firms', for example:
command: firms
date1:   12.03.2018
date2:   13.04.2018
id:      MB0139600000 
  
The application also allows you to get information about the activity of a particular user: the date and number of connections per day.
You should enter the command 'users', for example:
command: users
date1:   12.03.2018
date2:   13.04.2018
id:      MU0134700002

Results are output to the xlxs file.
The path of the input and output files you can specify in the configuration file.
You also need to specify the stand in the configuration file:'SP_CURR' or 'INET_FOND'.
