#from typing import List, Any, Union

import fdb
import xlsxwriter
import operator
import yaml


config = yaml.load(open('config.yml'))
input = yaml.load(open(config['IO_filenames']['input']))

class ActivityManager:
    def __init__(self, stand):
        if stand == ('INET_FOND'):
            #self.STAND_name = configuration_FOND_db
            self.STAND_name = config['stand_info']['INET_FOND']['db_path']
            self.STAND_login = config['stand_info']['INET_FOND']['db_login']
            self.STAND_pwd = config['stand_info']['INET_FOND']['db_pwd']
        elif stand == ('SP_CURR'):
            self.STAND_name = config['stand_info']['SP_CURR']['db_path']
            self.STAND_login = config['stand_info']['SP_CURR']['db_login']
            self.STAND_pwd = config['stand_info']['SP_CURR']['db_pwd']
        else:
            raise Exception('Unknown stand identificator')



        self.MON_db     = config['monitoring_info']['MON_db']
        self.MON_login  = config['monitoring_info']['MON_login']
        self.MON_pwd    = config['monitoring_info']['MON_pwd']

    def __get_users_by_firm(self, firm_id):
        #global CU_db, CU_login, CU_pwd
        # curr_con = fdb.connect(dsn="curr.gdb",user='CURASTS',password='curasts')
        curr_con = fdb.connect(dsn=self.STAND_name, user=self.STAND_login, password=self.STAND_pwd)
        cur = curr_con.cursor()
        cur.execute("select id from users where (id<>firmid) and  (firmid ='{0}')".format(firm_id))
        res = cur.fetchall()
        cur.close()
        curr_con.close()
        ll = list()
        for it in res:
            s = ''.join(it)
            ll.append(s)
        return ll

    def __get_user_in_period(self, user_id, date_1, date_2):
        mon_con = fdb.connect(dsn=self.MON_db, user=self.MON_login, password=self.MON_pwd)
        cur = mon_con.cursor()
        cur.execute(
            "select username from users_activity where (logon_date between '{1}' and '{2}') and (username = '{0}')".format(
                user_id, date_1, date_2))
        res = cur.fetchall()
        cur.close()
        mon_con.close()

        if (len(res) > 0):
            return True
        else:
            return False

    def __get_all_users_in_period(self, date_1, date_2):
        mon_con = fdb.connect(dsn=self.MON_db, user=self.MON_login, password=self.MON_pwd)
        cur = mon_con.cursor()
        cur.execute(
            "select distinct username from users_activity where logon_date between '{0}' and '{1}'".format(date_1,
                                                                                                           date_2))
        res = [row['USERNAME'] for row in cur.itermap()]

        cur.close()
        mon_con.close()
        return res

    def __get_firms_by_user(self, db_connection, user_name, debug=False):
        #global CU_db, CU_login, CU_pwd

        #curr_con = fdb.connect(dsn=self.CU_db, user=self.CU_login, password=self.CU_pwd)
        ## curr_con = fdb.connect(dsn="curr.gdb",user='CURASTS',password='curasts')

        cur = db_connection.cursor()

        if type(user_name) == str:
            query = "select firmid from users where (id<>firmid) and  id='{0}'".format(user_name)
        elif type(user_name) == list:
            query = "select firmid from users where (id<>firmid) and (id in('{0}'))".format("', '".join([str(row) for row in user_name]))

        if debug:
            print "query = {0}", query
        cur.execute(query)

        # cur.execute("select firmid from users where id='{0}'".format(user_name))
        res = cur.fetchall()
        if debug:
             for elem in res:
                print elem
        cur.close()
        #curr_con.close()
        if len(res):
            s = ''.join(res[0])
            return s
        else:
            return ''

    def __get_all_firms(self, db_connection):
       # curr_con = fdb.connect(dsn=self.CU_db, user=self.CU_login, password=self.CU_pwd)
        cur = db_connection.cursor()
        cur.execute("select id from firms order by id")
        res = cur.fetchall()
        cur.close()
        #curr_con.close()
        return res

    def __check_id(self, str):
        if (str[0].isalpha()):
            return 1
        else:
            return 0
    def __output2(self, filename, list1, list2, date1, date2):
        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(filename)
        worksheet = workbook.add_worksheet()


        # Write some numbers, with row/column notation.
        worksheet.write_string(0, 0, "period")
        worksheet.write_string(0, 1, date1)
        worksheet.write_string(0, 2, date2)
        worksheet.write_string(1, 0, "FIRM_ID")
        worksheet.write_string(1, 1, "ACTIVITY_STATUS")
        for i in range(len(list1)):
            decoded_str = list1[i].decode("windows-1252")
            encoded_str = decoded_str.encode('ascii', 'ignore')
            worksheet.write_string(i + 2, 0, encoded_str)
            worksheet.write_string(i + 2, 1, list2[i].decode('utf-8'))

        workbook.close()

    def __get_user_activity(self, user_id, period_1, period_2):
        curr_con = fdb.connect(dsn=self.MON_db, user=self.MON_login, password=self.MON_pwd)
        cur = curr_con.cursor()
        cur.execute(
            """select LOGON_DATE, LOGON_COUNT from users_activity
            where (username = '{0}') and (logon_date between '{1}' and '{2}')
            order by LOGON_DATE """.format(user_id, period_1, period_2))
        res = cur.fetchall()
        cur.close()
        curr_con.close()
        return res
    def __is_user_exists(self, uid):
        curr_con = fdb.connect(dsn=self.STAND_name, user=self.STAND_login, password=self.STAND_pwd)
        cur = curr_con.cursor()
        cur.execute("select ID from USERS where ID = '{0}'".format(uid));
        res = cur.fetchall()
        cur.close()
        curr_con.close()
        if( len(res) > 0):
            return 1
        else:
            return 0
    def __is_firm_exists(self, fid):
        curr_con = fdb.connect(dsn=self.STAND_name, user=self.STAND_login, password=self.STAND_pwd)
        cur = curr_con.cursor()
        cur.execute("select ID from FIRMS where ID = '{0}'".format(fid));
        res = cur.fetchall()
        cur.close()
        curr_con.close()
        if( len(res) > 0):
            return 1
        else:
            return 0

    def __check_filename(self, filename):
        if (len(filename) == 0):
            return -1
        else:
            return 0

    def F_ALL(self, date1, date2, fileToSave):
        if(self.__check_filename(fileToSave)):
            return -1
        user_id = self.__get_all_users_in_period(date1, date2)

        # Optimisation. One connection - two request
        curr_con = fdb.connect(dsn=self.STAND_name, user=self.STAND_login, password=self.STAND_pwd)
        firm_id = self.__get_firms_by_user(curr_con, user_id)
        all_l = self.__get_all_firms(curr_con)
        curr_con.close()

        all_l.sort()

        # Form xlsx file
        firm_activities = dict()
        for elem in all_l:
            s = ''.join(elem)
            if (self.__check_id(s)):
                if (s in firm_id):
                    firm_activities[s]='A'
                else:
                    firm_activities[s]='N'

        sorted_firm_activities = sorted(firm_activities.items(), key=operator.itemgetter(1))
        tmp=zip(*sorted_firm_activities)
        firms = tmp[0]
        activities = tmp[1]
        self.__output2(fileToSave, firms, activities, date1, date2)
        #print len(firm_id)
        return 0

    def F_USER(self, date1, date2, ID, fileToSave):
        if(self.__check_filename(fileToSave)):
            return -1
        user_exists = 0;
        if(self.__is_user_exists(ID) == 1):
            user_activity = self.__get_user_activity(ID, date1, date2)
            user_exists = 1;

        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(fileToSave)
        worksheet = workbook.add_worksheet()
        if(user_exists):
            # Write data to xlsx file
            worksheet.write(0, 0, "user_id")
            worksheet.write(1, 0, "period")
            worksheet.write(0, 1, ID)
            worksheet.write(1, 1, date1)
            worksheet.write(1, 2, date2)
            worksheet.write(2, 0, "DATE")
            worksheet.write(2, 1, "LOGON_COUNT")


            for i in range(len(user_activity)):
                worksheet.write(i + 3, 0, str(user_activity[i][0]))
                worksheet.write(i + 3, 1, user_activity[i][1])
        else:
            worksheet.write(0, 0, "specified user not exists")
        workbook.close()
        return 0

    def F_FIRM(self, date1, date2, ID, fileToSave):
        if(self.__check_filename(fileToSave)):
            return -1
        firm_exists = 0;
        if (self.__is_firm_exists(ID) == 1):
            firm_exists = 1;
            general_user_ids = self.__get_users_by_firm(ID)
            firm_activities = dict()
            for elem in general_user_ids:
                if (self.__get_user_in_period(elem, date1, date2)):
                    firm_activities[elem] = 'A'
                else:
                    firm_activities[elem] = 'N'
            sorted_firm_activities = sorted(firm_activities.items(), key=operator.itemgetter(1))

        # Create an new Excel file and add a worksheet.
        workbook = xlsxwriter.Workbook(fileToSave)
        worksheet = workbook.add_worksheet()
        if (firm_exists):
            # Write data to xlsx file
            worksheet.write(0, 0, "firm_id")
            worksheet.write(1, 0, "period")
            worksheet.write(0, 1, ID)
            worksheet.write(1, 1, date1)
            worksheet.write(1, 2, date2)
            worksheet.write(2, 0, "USER_ID")
            worksheet.write(2, 1, "ACTIVITY_STATUS")

            index = 0
            for elem in sorted_firm_activities:
                worksheet.write_string(index + 3, 0, elem[0])
                worksheet.write_string(index + 3, 1, elem[1])
                index += 1
        else:
            worksheet.write(0, 0, "specified user not exists")
        workbook.close()
        return 0


def test():
    #print(config['database_info']['CU_pwd'])

    actManager = ActivityManager(config['current_stand']['stand'])
    cmd = input['command']
    if (cmd == 'firms'):
        if not (actManager.F_FIRM(input['date1'], input['date2'], input['id'], config['IO_filenames']['output'])):
            print 'Test FIRM option success'
    elif(cmd == 'users'):
        if not (actManager.F_USER(input['date1'], input['date2'], input['id'], config['IO_filenames']['output'])):
            print 'Test USER option success'
    elif (cmd == 'all'):
        if not (actManager.F_ALL(input['date1'], input['date2'], config['IO_filenames']['output'])):
            print 'Test ALL option success'
<<<<<<< HEAD
    elif (cmd == 'all_stat'):
       # if not (actManager.F_ALL_STAT(input['date1'], input['date2'], config['IO_filenames']['output'])):
       print 'Test ALL_STAT option success'
=======
>>>>>>> 4c8d3d2df85e259c4623239eda9036b8fb96bdaa
    else:
        print 'No command found'
if __name__ == '__main__':
    test()

